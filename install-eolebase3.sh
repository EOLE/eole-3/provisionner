#!/bin/sh
# shellcheck disable=SC2034

WORKDIR=$(dirname "${0}")

Ubuntu_pkg_list="curl"

if ! OPTS=$(getopt -o e:s:a:m: --long engine:,server:,agent:,mode: -n provisionner -- "${@}"); then
  echo "${OPTS}"
  exit 1
fi

eval set -- "$OPTS"

KUBE_ENGINE="k3d"
K3D_TAG="v5.6.0"       # k3d version
K3S_TAG="v1.28.3-k3s2" # k3s version
DEPLOY_MODE="dev"      # deploy modes are "dev" and "production"
SERVER=3
AGENT=2
while true; do
  case "$1" in
    -m | --mode)
      DEPLOY_MODE="${2}"
      shift 2
      ;;
    -e | --engine)
      KUBE_ENGINE="${2}"
      shift 2
      ;;
    -s | --server)
      SERVER="${2}"
      shift 2
      ;;
    -a | --agent)
      AGENT="${2}"
      shift 2
      ;;
    --)
      shift
      break
      ;;
    *) break ;;
  esac
done

#
# @NAME: errMsg
# @AIM: Error message formater
# @PARAMS:
#   - The message
#   - The return code (0 for success)
# @RETURN: integer (0 for success)
#
errMsg() {
  message=${1}
  errCode=${2}
  echo
  echo "[ERROR] ${message}"
  if [ -z "${errCode}" ]; then
    errCode=1
  fi
  return "${errCode}"
}

#
# @NAME: installAptPkgs
# @AIM: Install packages with apt
# @PARAMS: a list of packages names
# @RETURN: integer (0 for success)
#
installAptPkgs() {
  timeout=3600
  printf "   - Repository update "
  if ! DEBIAN_FRONTEND=noninteractive flock --timeout 60 --exclusive --close /var/lib/dpkg/lock-frontend apt-get update </dev/null >/dev/null; then
    errMsg "Package repository update failed !" 3
    return ${?}
  fi
  printf '\n'

  for pkg in "${@}"; do
    echo "   - ${pkg}"
    if ! DEBIAN_FRONTEND=noninteractive apt-get install -y -qq "${pkg}" </dev/null >/dev/null; then
      errMsg "${pkg} install failed !" 2
      return ${?}
    fi
  done
  return 0
}

#
# @NAME: installPkgs
# @AIM: Wrapper for packages installation
# @PARAMS: a list of packages names
# @RETURN: integer (0 for success)
#
installPkgs() {
  case ${DISTRIB_ID} in
    Ubuntu | Debian)
      echo "${DISTRIB_DESCRIPTION}:"
      installAptPkgs "${@}"
      return $?
      ;;
    *)
      errMsg "Unsupported OS [${DISTRIB_ID}]" 10
      return ${?}
      ;;
  esac
}

#
# @NAME: setupSSH
# @AIM: Update minimal configuration for SSH Daemon
# @PARAMS: None
# @RETURN: integer (0 for success)
#
setupSSH() {
  case ${DISTRIB_ID} in
    Ubuntu | Debian)
      echo "   - root login"
      sed -i -e 's/#PermitRootLogin.*/PermitRootLogin yes/' /etc/ssh/sshd_config
      echo "   - agent forwarding"

      sed -i -e 's/#AllowAgentForwarding.*/AllowAgentForwarding yes/' /etc/ssh/sshd_config
      echo "   - daemon restart"
      systemctl restart ssh
      return ${?}
      ;;
    *)
      errMsg "Unsupported OS [${DISTRIB_ID}]" 12
      return ${?}
      ;;
  esac
}

#
# @NAME: install_bash_completions
# @AIM: Install bash completions for kubectl, helm, k3d....
# @PARAMS: None
# @RETURN: integer (0 for success)
#
install_bash_completions() {
  mkdir -p /etc/bash_completion.d/
  kubectl completion bash >/etc/bash_completion.d/kubectl
  helm completion bash >/etc/bash_completion.d/helm

  touch ~root/.screenrc
  if ! grep -q 'startup_message off' ~root/.screenrc; then
    cat >>~root/.screenrc <<EOF
# Don't display the copyright page
startup_message off
EOF
  fi

  if ! grep -q 'defshell -bash' ~root/.screenrc; then
    cat >>~root/.screenrc <<EOF

# To enable bash completion
defshell -bash
EOF
  fi

  if ! grep -q 'defscrollback ' ~root/.screenrc; then
    cat >>~root/.screenrc <<EOF

# keep scrollback n lines
defscrollback 1000
EOF
  fi
}

#
# @NAME: installTools
# @AIM: Install kubernetes tools (helm....)
# @PARAMS: None
# @RETURN: integer (0 for success)
#
installTools() {
  case ${DISTRIB_ID} in
    Ubuntu | Debian)
      # Install Docker
      installPkgs snapd apt-transport-https
      res=${?}
      if [ ${res} -ne 0 ]; then
        errMsg "Error installing docker.io or snapd" 3
        return ${res}
      fi

      # Install kubectl
      out=$(snap install kubectl --stable --classic 2>&1 >/dev/null)
      res=${?}
      if [ ${res} -ne 0 ]; then
        errMsg "Error installing kubectl"
        return ${res}
      fi

      # Install helm
      curl -Ss https://baltocdn.com/helm/signing.asc | gpg --dearmor | tee /usr/share/keyrings/helm.gpg >/dev/null
      echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" >/etc/apt/sources.list.d/helm-stable-debian.list
      echo
      printf " Installing Helm for "
      installPkgs helm

      # Install bash completions
      install_bash_completions

      return 0
      ;;
    *)
      errMsg "Unsupported OS ${DISTRIB_ID}" 12
      return ${?}
      ;;
  esac
}

#
# @NAME: install_k3s
# @AIM: Install k3s+k3s software
# @PARAMS: None
# @RETURN: integer (0 for success)
#
install_k3s() {
  install_dir="/usr/bin"
  k3s_exec_opts=""
  # Install K3s
  install_script="https://get.k3s.io/"
  out=$(wget -O install.sh ${install_script} 2>&1 >/dev/null)
  res=${?}
  if [ ${res} -ne 0 ]; then
    errMsg "Error downloading k3d install script" 4
    return ${res}
  fi

  k3s_exec_opts="--disable=traefik"

  out=$(INSTALL_K3S_VERSION="${K3S_TAG}" INSTALL_K3S_BIN_DIR="${install_dir}" INSTALL_K3S_EXEC="${k3s_exec_opts}" sh install.sh 2>&1)
  res=${?}
  if [ ${res} -ne 0 ]; then
    errMsg "Error installing k3s"
    return ${res}
  fi

  systemctl enable --now k3s.service

  return 0
}

#
# @NAME: install_k3d
# @AIM: Install k3d+k3s software
# @PARAMS: None
# @RETURN: integer (0 for success)
#
install_k3d() {
  case ${DISTRIB_ID} in
    Ubuntu | Debian)
      # Install K3D
      installPkgs docker.io
      install_script="https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh"
      out=$(wget -O install.sh ${install_script} 2>&1 >/dev/null)
      res=${?}
      if [ ${res} -ne 0 ]; then
        errMsg "Error downloading k3d install script" 4
        return ${res}
      fi
      out=$(TAG="${K3D_TAG}" bash install.sh 2>&1)
      res=${?}
      if [ ${res} -ne 0 ]; then
        errMsg "Error installing k3d"
        return ${res}
      fi

      # Install bash completions
      k3d completion bash >/etc/bash_completion.d/k3d

      return 0
      ;;
    *)
      errMsg "Unsupported OS ${DISTRIB_ID}" 12
      return ${?}
      ;;
  esac
}

#
# @NAME: installEngine
# @AIM: Install kubernetes software
# @PARAMS: engine name
# @RETURN: integer (0 for success)
#
installEngine() {
  if ! installTools; then
    return "${?}"
  fi
  case "${1}" in
    k3d)
      install_k3d
      return "${?}"
      ;;
    k3s)
      install_k3s
      return "${?}"
      ;;
    *)
      errMsg "Unsupported kubernetes engine ${1}" 13
      return ${?}
      ;;
  esac
}

#
# @NAME: configure_k3d_daemon
# @AIM: Configure k3d .service
# @PARAMS: Cluster name (string)
# @RETURN: integer (0 for success)
#
configure_k3d_daemon() {
  name=${1}
  # Template unit
  echo "   - Create systemd service k3d@${name}.service"
  cat >/etc/systemd/system/k3d@.service <<EOF
[Unit]
After=docker.service
Wants=docker.service
ConditionPathExists=/usr/local/bin/k3d

[Service]
Type=oneshot
TimeoutSec=5min
RemainAfterExit=yes
ExecStart=/usr/local/bin/k3d cluster start %I
ExecStop=/usr/local/bin/k3d cluster stop %I

[Install]
WantedBy=multi-user.target
EOF

  ln -nfs /etc/systemd/system/k3d@.service "/etc/systemd/system/k3d@${name}.service"
  systemctl daemon-reload
  # Stop cluster before starting it with systemd
  out=$(k3d cluster stop "${name}" 2>&1)
  # Disable service if it is already enabled
  systemctl disable --now "k3d@${name}.service" >/dev/null 2>&1

  echo "   - Enable k3d@${name}.service"
  systemctl enable --now "k3d@${name}.service"
}

#
# @NAME: setup_k3s
# @AIM: Configure k3s
# @PARAMS: None
# @RETURN: integer (0 for success)
#
setup_k3s() {
  echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >>"${HOME}/.profile"
}

#
# @NAME: setup_k3d
# @AIM: Configure k3d software
# @PARAMS: cluster name
# @RETURN: integer (0 for success)
#
setup_k3d() {
  cname="${1}"
  storage_root="/srv/k3d/storage"

  if [ ! -d "${storage_root}" ]; then
    mkdir -p "${storage_root}"
  fi

  output=""
  # shellcheck disable=SC2086
  output=$(k3d cluster create "${cname}" --config eole3.yaml 2>&1)
  res=${?}
  if [ ${res} -eq 0 ]; then
    echo "   - Cluster ${cname} created"
  else
    echo
    echo "${output}"
    return ${res}
  fi
}

#
# @NAME: setupEngine
# @AIM: Configure kubernetes software
# @PARAMS: engine name
# @RETURN: integer (0 for success)
setupEngine() {
  cname="eole3"
  case ${1} in
    k3d)
      if ! setup_k3d "${cname}"; then
        return "${?}"
      fi
      configure_k3d_daemon "${cname}"
      return "${?}"
      ;;
    k3s)
      setup_k3s
      return "${?}"
      ;;
    *)
      errMsg "Unsupported kubernetes engine ${1}" 13
      return ${?}
      ;;
  esac
}

if lsb_release >/dev/null 2>&1; then
  DISTRIB_ID=$(lsb_release --short --id)
  DISTRIB_RELEASE=$(lsb_release --short --release)
  DISTRIB_DESCRIPTION=$(lsb_release --short --description)
else
  errMsg "Command lsb_release is missing : Unsupported OS"
  exit 2
fi

case "${DEPLOY_MODE}" in
  dev | production)
    echo "Deployement mode : ${DEPLOY_MODE}"
    ;;
  *)
    errMsg "Deploy mode \"${DEPLOY_MODE}\" is not \"dev\" or \"production\"" 3
    exit "${?}"
    ;;
esac

echo "*********************************************************"
echo "*                 Eolebase3 Provisionner                *"
echo "*********************************************************"

if [ "${DEPLOY_MODE}" = "dev" ]; then
  # SSH Setup
  echo " SSH daemon setup (Dev mode only)"
  setupSSH
  echo
fi

# Installing packages
printf " Package installation for "
pkg_list=$(eval "echo \${${DISTRIB_ID}_pkg_list}")
# shellcheck disable=SC2086
if ! installPkgs ${pkg_list}; then
  errMsg "Packages installation failed for ${DISTRIB_ID}" 4
  exit ${?}
fi

echo

if [ "${DEPLOY_MODE}" = "production" ]; then
  KUBE_ENGINE="k3s"
fi

# Installing Kubernetes
printf " Installing kubernetes engine %s" "${KUBE_ENGINE} "
if ! installEngine "${KUBE_ENGINE}"; then
  errMsg "${KUBE_ENGINE} installation failed !" 5
  exit "${?}"
fi

# Setup engine
echo
echo " Setup ${KUBE_ENGINE}"

if ! setupEngine "${KUBE_ENGINE}"; then
  errMsg "Error during engine ${KUBE_ENGINE} setup"
  echo
  echo "*********************************************************"
  exit 1
fi
echo
echo "*********************************************************"
